﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ThingsWebApi.Controllers {
    [Route("source")]
    [ApiController]
    public class ApiController : ControllerBase {
        #region Mock data

        static List<Room> _rooms = new List<Room> {
            new Room("Living room"),
            new Room("Bedroom"),
            new Room("Kitchen")
        };

        static List<Place> _placesInLivingRoom = new List<Place> {
            new Place("Bookcase"),
            new Place("Coffee table"),
            new Place("Side table")
        };
        static List<Place> _placesInBedroom = new List<Place> {
            new Place("Bedstand"),
            new Place("Closet")
        };
        static List<Place> _placesInKitchen = new List<Place> {
            new Place("Counter"),
            new Place("Upper cabinets"),
            new Place("Lower cabinets")
        };

        static List<List<Place>> _places = new List<List<Place>> {
            _placesInLivingRoom,
            _placesInBedroom,
            _placesInKitchen
        };

        static List<List<List<Thing>>> _things = new List<List<List<Thing>>> {
            // living room 
            new List<List<Thing>> {
                new List<Thing> { new Thing("M*A*S*H", ThingType.Video), new Thing("War And Peace", ThingType.Book)},
                new List<Thing> { new Thing("Glass art vase", ThingType.Other), new Thing("Picture album", ThingType.Memento)},
                new List<Thing> { new Thing("Box of CDs", ThingType.Music)}
            },
            // bedroom 
            new List<List<Thing>> {
                new List<Thing> { new Thing("Clock", ThingType.Tool), new Thing("Lamp", ThingType.Tool)},
                new List<Thing> {
                    new Thing("Shirts", ThingType.Clothing),
                    new Thing("Shorts", ThingType.Clothing),
                    new Thing("Blankets", ThingType.Other)
                }
            },
            // kitchen 
            new List<List<Thing>> {
                new List<Thing> { new Thing("Cleaning spray", ThingType.Supply),
                    new Thing("Paper towels", ThingType.Supply),
                    new Thing("Coffee maker", ThingType.Tool)
                },
                new List<Thing> { new Thing("Canned soup", ThingType.Food),
                    new Thing("Dried vegetables", ThingType.Food),
                    new Thing("Cat food", ThingType.Other)
                },
                new List<Thing> { new Thing("Pots", ThingType.Cooking),
                    new Thing("Pans", ThingType.Cooking),
                    new Thing("Silverware", ThingType.Other),
                    new Thing("Cups", ThingType.Other)
                }
            }
        };

        #endregion Mock data


        #region Http-get methods

        [HttpGet]
        public ActionResult<string> Get() {
            string text = @"
        <div style=""font-size: 150%; font-family: sans-serif"">
            API calls:
        </div>
        <div style = ""padding-left: 10%; font-size: 125%; font-family: sans-serif"">
                 <br/>
                 get:/source/getRooms
                 <br/>
                 get:/source/getPlacesInRoom/room/[room name]
                 <br/>
                 get:/source/getThingsInPlaceInRoom/room/[room name]/place/[place name]
                 <br/>
                 post:/source/addThingToPlaceInRoom + { thing, place, room }
        </div>";

            return text;
        }

        [HttpGet("/source/getRooms")]
        public ActionResult<List<Room>> GetRooms() {
            return _rooms;
        }

        [HttpGet("/source/getPlacesInRoom/room/{room}")]
        public ActionResult<List<Place>> GetPlacesInRoom(string room) {
            int index = _rooms
                .FindIndex(x => x.Name == room);

            List<Place> places = _places[index];
            return places;
        }

        [HttpGet("getThingsInPlaceInRoom/room/{room}/place/{place}")]
        public ActionResult<List<Thing>> GetThingsInPlaceInRoom(string place, string room) {
            return GetThingsFromPlaceAndRoom(place, room);
        }

        #endregion Http-get methods


        #region Http-post, -patch, and -delete methods

        [HttpPost("/source/addThingToPlaceInRoom")]
        public ActionResult<bool> AddThingToPlaceInRoom(ThingPostBundle bundle) {
            Thing thing = bundle.thing;
            Place place = bundle.place;
            Room room = bundle.room;

            string roomName = room.Name;
            string placeName = place.Name;

            List<Thing> things = GetThingsFromPlaceAndRoom(placeName, roomName);
            things.Add(thing);

            return true;
        }

        #endregion Http-post, -patch, and -delete methods


        #region Shared dependencies

        private List<Thing> GetThingsFromPlaceAndRoom(string place, string room) {
            int inRoom = _rooms
                .FindIndex(x => x.Name == room);

            int atPlace = _places[inRoom]
                .FindIndex(x => x.Name == place);

            List<Thing> things = _things[inRoom][atPlace];
            return things;
        }

        #endregion Shared dependencies

    }
}
