﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ThingsWebApi {
    public class ThingType {
        public const string Book = "Book";
        public const string Supply = "Supply";
        public const string Clothing = "Clothing";
        public const string Food = "Food";
        public const string Cooking = "Cooking";
        public const string Tool = "Tool";
        public const string Video = "Video";
        public const string Music = "Music";
        public const string Memento = "Memento";
        public const string Other = "Other";
        public const string Unknown = "Unknown";
    }
}
