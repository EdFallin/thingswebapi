﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ThingsWebApi {
    public class Thing {
        public string Name { get; set; }

        public string Type { get; set; }  // from the ThingType constants 

        public Thing(string name, string type) {
            Name = name;
            Type = type;
        }
    }
}
