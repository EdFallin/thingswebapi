﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ThingsWebApi {
    public class ThingPostBundle {
        public Thing thing { get; set; }
        public Place place { get; set; }
        public Room room { get; set; }
    }
}
