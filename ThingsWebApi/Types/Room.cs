﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ThingsWebApi {
    public class Room {
        public string Name { get; set; }
        public Room(string name) {
            Name = name;
        }
    }
}
