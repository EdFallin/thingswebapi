﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ThingsWebApi {
    public class Place {
        public string Name { get; set; }

        public Place(string name) {
            Name = name;
        }
    }
}
